# git-muffle

Suppress the amount of timestamp metadata git stores in commits.

## Usage

### New commits:

    $ git muffle commit -m 'Add git-muffle-filter'
    $ git log --format=fuller -1
    commit 43c0f0c9b81b04cbc08d05e4501d99f498ded0be
    Author:     Sampo Sorsa <sorsasampo@protonmail.com>
    AuthorDate: Thu Jan 25 00:00:00 2018 +0000
    Commit:     Sampo Sorsa <sorsasampo@protonmail.com>
    CommitDate: Thu Jan 25 00:00:00 2018 +0000
    
        Add git-muffle-filter

### Old commits

    $ git filter-branch --env-filter '. /path/to/git-muffle-filter'

## Installation

Clone this repository and symlink `git-muffle` to `~/bin/`.

## Configuration

Both `git-muffle` and `git-muffle-filter` use `git config` option
`muffle.dateformat`. You can use the following command to modify the default:

    $ git config --global muffle.dateformat "%Y-%m-%dT00:00:00Z"

The default (above) is to clear the time and set timezone to UTC.

You can also change the timezone:

    $ git config --global muffle.timezone "UTC+0"

See [tzset(3)](https://manpages.debian.org/jump?q=tzset) for the syntax.

## See also

The following projects may also be of interest:

* [git-redate](https://github.com/PotatoLabs/git-redate)
* [git-shift](https://github.com/gitbits/git-shift/)
* [libfaketime](https://github.com/wolfcw/libfaketime)
